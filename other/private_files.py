""" Some files which are useless for others """

# I need this replacement, because I replaced it before to improve readability of the titles -.-
replacement_dict_martin = {
        'FDOM': '', 'DOM': '', 'CDOM': '',
        'EEM': '', 'PARAFAC': '',
        'DOC': '', 'TDOC': '',
        'DON': '',
        'NOM': '',
        'SVM': '', 'SOM': '', 'RFR': '', 'NN': '',
        'PCR': '',
        'BOD': '', 'COD': '',
    }