import bibtexparser
import requests
import string
from pylatexenc.latex2text import LatexNodes2Text

with open('demo.bib') as f:
    bib = bibtexparser.load(f)

for b in bib.entries:
    # We need to convert the LaTeX to plain text and remove punctuation
    title = LatexNodes2Text().latex_to_text(b.get('title')).translate(str.maketrans('', '', string.punctuation))
    print(f"### {title} ###")

    # - 1. Get citation counts from OpenAlex

        # OpenAlex is orientied towards persistent identifiers (ORCiD, DOI, etc.),
        # so search functionality is somewhat limited. If you have DOIs for your
        # publications, this is probably the best option right now.

    # -- 1.1 Search by title

        # This will usually yield multiple results due to fuzzy search

    print("-- Searching by title in OpenAlex")

    query_url = f'https://api.openalex.org/works?filter=title.search:{title}'
    response = requests.get(query_url).json()
    try:
        print(f"{} search results")
        for result in response['results'][:3]:
            # I am showing the first three results here, but you could either
            # (a) trust the search and use the first result
            # (b) use some further checks (like comparing authors) to find the correct one
            print(f"{result['id']}:\t{result['cited_by_count']} citations")
    except KeyError as e:
        print("No search results")


    # -- 1.2 Search by DOI
        # In this case, you can simply retrieve them by DOI: https://docs.openalex.org/api/get-single-entities
        # and then get cited_by_count from the results

    # - 2. Get citation counts via Scopus

        # AWI actually subscribes to Scopus, so you should be able to obtain an API key here:
        # http://dev.elsevier.com/myapikey.html - as described in the pybliometrics docs (https://pybliometrics.readthedocs.io/en/stable/)

    from pybliometrics.scopus import ScopusSearch

    print ("-- Searching by title in Scopus")

    s = ScopusSearch(f"TITLE ( {title} )")
    if s.results:
        print(f"{len(s.results)} search results")
        for result in s.results[:3]:
            # Again, just using the first three results
            # Scopus search seems less reliable in picking the correct ones than OpenAlex,
            # but you could probably also do more filtering based on authors etc.
            print(f"{result.eid}:\t{result.citedby_count} citations")
    else:
        print("No search results")