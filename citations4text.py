""" Load a text file and get number of citations """

from apis.api_methods import get_citation_data_from_file

from other.private_files import replacement_dict_martin


def save_number_of_cites4text(file_name: str, api_method: str = 'OpenAlex', replacement_dict: dict = None) -> None:
    """ Load a text file and get number of citations

    Warning: the use of cache can lead to outdated results
    """

    # get citation data
    citation_dicts = get_citation_data_from_file(file_name=file_name,
                                                 api_method=api_method,
                                                 replacement_dict=replacement_dict)

    # this is awful, I know...
    with open(file_name.replace('.txt', '') + '_citations.txt', 'w') as f_cite:
        with open(file_name.replace('.txt', '') + '_author.txt', 'w', encoding="utf-8") as f_author:
            with open(file_name.replace('.txt', '') + '_title.txt', 'w', encoding="utf-8") as f_title:
                with open(file_name.replace('.txt', '') + '_score.txt', 'w') as f_score:

                    for citation_dict in citation_dicts:

                        if citation_dict is None:
                            num_citations = -1
                            citation_author = 'NA'
                            citation_title = 'NA'
                            citation_score = 'NA'
                        else:
                            num_citations = citation_dict['number_of_citations']
                            citation_author = citation_dict['author']
                            citation_title = citation_dict['title']
                            if api_method == 'OpenAlex':
                                citation_score = citation_dict['OA_relevance_score']
                            else:
                                citation_score = 'unknown'

                        print('{} citations for "{}".'.format(num_citations, prepared_title))
                        f_cite.write('{}\n'.format(num_citations))
                        f_author.write('{}\n'.format(citation_author))
                        f_title.write('{}\n'.format(citation_title))
                        f_score.write('{}\n'.format(citation_score))


# testing
if __name__ == '__main__':

    save_number_of_cites4text('demo.txt', replacement_dict=replacement_dict_martin)

    print('Done')
