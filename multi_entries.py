import pandas as pd

from single_entries import BibtexSingle


class BibtexMulti(BibtexSingle):

    @property
    def authors(self) -> dict:
        """
        Scan BibTex File for all authors.
        :return: dict with author names and assigned keys
        """
        author_dict = dict()
        for entry in self.entries:
            authors = self.get_authors(entry)
            for author in authors:
                if author in author_dict:
                    author_dict[author].append(entry)
                else:
                    author_dict[author] = [entry]
        return author_dict

    def get_fields(self, key_field: str) -> list:
        return [self.get_field(bibtex_key, key_field) for bibtex_key in self.entries]

    @staticmethod
    def _convert_frame_types(df: pd.DataFrame, columns: list = None) -> pd.DataFrame:
        """
        Try to convert string columns to float columns.
        Will print problems to console.
        :param df: DataFrame
        :param columns: List with column names
        :return: modified DataFrame
        """
        if columns is None:
            columns = ['year', 'number', 'volume']

        for column in columns:
            try:
                df = df.astype({column: 'float'})
            except ValueError as err:
                print('{column}: {error}'.format(column=column, error=err))

        return df

    def get_dataframe(self) -> pd.DataFrame:
        df = pd.DataFrame()
        for entry in self.entries:
            df = df.append(pd.DataFrame(self.entries[entry], index=[entry]))
        return self._convert_frame_types(df)


# testing
if __name__ == '__main__':

    test = BibtexMulti('demo.bib')

    print('Overview of authors: {}'.format(test.authors))
    print('Stored Years: {}'.format(test.get_fields('year')))

    print('Here comes a complete overview:')
    test_frame = test.get_dataframe()
    print(test_frame)

    print('Done')
