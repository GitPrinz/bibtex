""" Create a Graph with author - paper relations

Example works with demo.txt

"""

import re

from collections import Counter

from apis.api_methods import get_citation_data_from_file
from apis.api_base import strip_anything

from other.private_files import replacement_dict_martin


def save_relations_markdown(file_name: str,
                            replacement_dict: dict = None,
                            full_paper_name: bool = False,
                            author_occurrence: int = 1,
                            referenced_papers: bool = False,
                            referenced_authors: bool = False,
                            referenced_occurrence: int = 1,
                            citing_papers: bool = False,
                            citing_authors: bool = False,
                            citing_occurrence: int = 1) -> None:
    """ Create a Graph with author - paper relations

    - load data from titles
    - get referenced literature (this may take a while)
    - get citing literature (this may take a while)
    - write .md file with mermaid graph

    Warning: the use of cache can lead to outdated results

    :param file_name: text file with titles
    :param replacement_dict: dict with replacements in title
    :param full_paper_name: write full paper names in graph (not recommended)
    :param author_occurrence: author with fewer occurrences will not be shown
    :param referenced_papers: add referenced papers
    :param referenced_authors: add authors of referenced papers
    :param referenced_occurrence: fewer referenced papers will not be shown
    :param citing_papers: add citing papers
    :param citing_authors: add authors of citing papers
    :param citing_occurrence: citing paper with fewer occurrences will not be shown

    """

    # get citation data #
    #####################
    citation_dicts = get_citation_data_from_file(file_name=file_name,
                                                 api_method='OpenAlex',
                                                 replacement_dict=replacement_dict,
                                                 referenced_papers=referenced_papers,
                                                 citing_papers=citing_papers)

    # Combine Data #
    ################

    # prepare mermaid lines
    paper_lines = []
    author_lines = []
    link_lines = []
    referenced_lines = []
    citing_lines = []

    # prepare counters
    authors_list = []
    referenced_list = []
    citing_list = []

    def get_author_id(aut_name: str) -> str:
        return strip_anything(aut_name).replace(' ', '')

    def add_authors(author_list: list, aut_style: str) -> None:
        for aut_name in author_list:
            author_id = get_author_id(aut_name)
            authors_list.append(author_id)
            author_lines.append('{aut_id}{{{aut_nam}}}:::{aut_style}\n'.format(  # see filters when changing
                aut_id=author_id,
                aut_nam=strip_anything(aut_name),
                aut_style=aut_style))
            link_lines.append('{aut_id} ==> {pap_id}\n'.format(  # see filters when changing
                aut_id=author_id,
                pap_id=citation_dict['bibtex_key']))

    for citation_dict in citation_dicts:

        if citation_dict is None:
            continue

        if full_paper_name:
            # define papers
            paper_lines.append('{pap_id}[{title}]\n'.format(
                pap_id=citation_dict['bibtex_key'],
                title=strip_anything(citation_dict['title'])))
            # todo: referenced, citing

        # add authors and links to papers
        add_authors(citation_dict['authors'], aut_style='aut')

        if referenced_papers:
            for ref_pap in citation_dict['referenced_papers']:
                if ref_pap['authors'] == 'NA':
                    continue
                referenced_list.append(ref_pap['bibtex_key'])
                referenced_lines.append('{ref_id}:::ref --> {pap_id}\n'.format(  # see filters when changing
                    ref_id=ref_pap['bibtex_key'],
                    pap_id=citation_dict['bibtex_key']))
                if referenced_authors:
                    add_authors(ref_pap['authors'], aut_style='ref')

        if citing_papers:
            for cit_id in citation_dict['citing_papers']:
                citing_list.append(cit_id['bibtex_key'])
                citing_lines.append('{pap_id} .-> {cit_id}:::cit\n'.format(  # see filters when changing
                    cit_id=cit_id['bibtex_key'],
                    pap_id=citation_dict['bibtex_key']))
                if citing_authors:
                    add_authors(cit_id['authors'], aut_style='cit')

    # Organize Data #
    #################

    def print_ranking(f_name: str, count_dict: dict) -> None:
        """ Write ranking in file """
        lab_els, cou_nts = zip(*[(key, count_dict[key]) for key in count_dict])

        with open(f_name + '_ranking.txt', 'w', encoding="utf-8") as fid:

            for lab, cou in [(lab, cou) for cou, lab in sorted(zip(cou_nts, lab_els), reverse=True)]:
                fid.write('{cou}: {lab}\n'.format(cou=cou, lab=lab))

        return

    # find relevant entries
    def return_multiple_occurrences(item_list: list, min_occurrence: int = 2,
                                    f_name: str = None) -> set:
        """ Filter occurrences from list and print them (yes, bad function design) """
        count_dict = Counter(item_list)

        if file_name is not None:
            print_ranking(f_name=f_name, count_dict=count_dict)

        return {it_em for it_em in count_dict if count_dict[it_em] >= min_occurrence}

    authors_set = return_multiple_occurrences(authors_list, author_occurrence,
                                              f_name=file_name.replace('.txt', '') + '_author')
    referenced_set = return_multiple_occurrences(referenced_list, referenced_occurrence,
                                                 f_name=file_name.replace('.txt', '') + '_references')
    citing_set = return_multiple_occurrences(citing_list, citing_occurrence,
                                             f_name=file_name.replace('.txt', '') + '_citing')

    # remove others
    #   by Brute Force ... see creation of lines and update both together
    author_lines = [aut_line for aut_line in author_lines if re.findall(
        r'(\w*){.*}:::.*', aut_line)[0] in authors_set]
    link_lines = [lin_line for lin_line in link_lines if re.findall(
        r'(\w*) ==> \w*', lin_line)[0] in authors_set]
    referenced_lines = [ref_line for ref_line in referenced_lines if re.findall(
        r'(\w*):::ref --> \w*', ref_line)[0] in referenced_set]
    citing_lines = [cit_line for cit_line in citing_lines if re.findall(
        r'\w* .-> (\w*):::cit', cit_line)[0] in citing_set]

    # Sorting data and removing duplicates
    paper_lines = sorted(set(paper_lines))
    author_lines = sorted(set(author_lines))
    link_lines = sorted(set(link_lines))
    referenced_lines = sorted(set(referenced_lines))
    citing_lines = sorted(set(citing_lines))

    # write everything in File #
    ############################

    def_aut = 'classDef aut fill:yellow;'
    def_ref = 'classDef ref fill:skyblue;'
    def_cit = 'classDef cit fill:limegreen;'

    with open(file_name.replace('.txt', '') + '_relations.md', 'w', encoding="utf-8") as f_id:

        f_id.write('# Author relations\n')

        # add explanation #
        ###################

        f_id.write('```mermaid\n'
                   'graph LR\n\n')

        f_id.write('{}\n{}\n{}\n\n'.format(def_aut, def_ref, def_cit))
        f_id.write('author{{Author Name {}+ occurrences}}:::aut ==> paper[Paper]\n'.format(author_occurrence))
        f_id.write('referenced[Referenced Paper {}+ occurrences]:::ref --> paper[Paper]\n'.format(referenced_occurrence))
        f_id.write('paper[Paper] .-> citing[Citing Paper {}+ occurrences]:::cit\n'.format(citing_occurrence))

        f_id.write('\n```\n')

        # add main graph #
        ##################

        f_id.write('```mermaid\n'
                   'graph LR\n\n')

        if full_paper_name:
            f_id.write('%% Paper definitions\n')
            f_id.writelines(paper_lines)

        f_id.write('%% Author definitions\n')
        f_id.writelines(author_lines)

        f_id.write('\n%% Author Paper links\n{}\n'.format(def_aut))
        f_id.writelines(link_lines)

        if referenced_papers:
            f_id.write('\n%% Referenced Papers\n{}\n'.format(def_ref))
            f_id.writelines(referenced_lines)

        if citing_papers:
            f_id.write('\n%% Citing Papers\n{}\n'.format(def_cit))
            f_id.writelines(citing_lines)

        f_id.write('\n```\n')

    return


# testing
if __name__ == '__main__':

    # Warning: Example can take hours to load (minutes when cached)
    # Warning: mermaid has a pretty small limit for amount of content

    save_relations_markdown('demo.txt', replacement_dict=replacement_dict_martin,
                            referenced_papers=True, citing_papers=True,
                            author_occurrence=3, referenced_occurrence=14, citing_occurrence=6)

    print('Done')
