from access import BibtexFile


class BibtexSingle(BibtexFile):

    def get_field(self, bibtex_key: str, key_field: str) -> str:
        if bibtex_key not in self.entries:
            raise Exception('Bibtexkey "{}" is unknown!'.format(bibtex_key))
        if key_field not in self.entries[bibtex_key]:
            return ""
        return self.entries[bibtex_key][key_field]

    def get_authors(self, bibtex_key: str) -> list:
        if bibtex_key not in self.entries:
            raise Exception('Bibtexkey "{}" is unknown!'.format(bibtex_key))
        return self.get_field(bibtex_key, 'author').split(' and ')


# testing
if __name__ == '__main__':

    test = BibtexSingle('demo.bib')

    print('The Author of oliphant2007python is "{}".'.format(test.get_field('oliphant2007python', 'author')))
    print('The Authors of kopka1991latex are "{}".'.format(test.get_authors('kopka1991latex')))

    print('Done')
