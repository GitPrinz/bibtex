# Literature (BibTex) API

Some simple Python scripts for my needs.
**Be aware everything can and probably will change someday.**

BibTex related

- Get entries and fields from BibTex
- Author overview
- Convert to Pandas Dataframe

General features

- Access some database to get further information
  - Google Scholar (not working right now)
  - OpenAlex (Thanks to [Andreas Walker](https://orcid.org/0000-0002-1541-122X) for the demonstration of the OpenAlex and the Scopus API.))
- Automatically creating overviews of probably relevant papers and authors for multiple papers (currently by paper titles)


I also made a draft for a Visualization based on this Repository: https://gitlab.com/GitPrinz/bibvis

Most files execute an example if they are run directly.

## Idea

I looked for a solution to add and update number of citations in BibTex entries.
In longterm I thought about visualizing interconnections between entries.

It was not planed to be something bigger and just fit my needs.

### Other Approaches

There are definitely other projects doing similar things.
Probably further developed. I haven't spent much time looking after an initial search.
If you know something related, I am happy to list it here!

- [graph_coaut_bibtex](https://github.com/jdmorise/graph_coaut_bibtex) Co-Author graph generator on BibTex Files
- [connectedpapers.com](https://www.connectedpapers.com/) **Very nice tool to find and visualize related papers!**

## APIs to Databases

I tried `scholarly` but this runs in access problems, since Google is blocking connections.
**Google doesn't want API access! Be prepared for problems.** 
The package is still in developement, and right now it is working.

As Alternative I looked into Web of Science, but there is no free interface.

[Andreas Walker](https://orcid.org/0000-0002-1541-122X) suggested OpenAlex, which works well.
There are less citations than in Google Scholar, but probably the quality is higher.
The API is just slow if a lot of requestes are necessary.

## Structure (maybe outdated)

```mermaid
graph LR

BibtexFile --> BibtexSingle
BibtexSingle --> BibtexMulti
BibtexSingle --> BibtexAPI

BibtexAPI --> wos(Web of Science)
BibtexAPI --> google(Google scholar)
BibtexAPI --> alex(Open Alex)

BibtexMulti --> BibtexFiltered
```



- [Access](access.py) (BibtexFile): Selecting BibTex file and providing data.
- [Single Entries](single_entries.py) (BibtexSingle): Accessing basic information of single entries.
    - [Web of Science](apis/webofscience.py): Not working
    - [Google Scholar](apis/scholar.py): Very Slow, Might Cause Problems
    - [**Open Alex**](apis/openalex.py): Open Source, Slow, Less citations
	- [OpenAlex, Scopus demonstration](other/demo_walker.py): Kudos to [Andreas Walker](https://orcid.org/0000-0002-1541-122X)
- [Multi Entries](multi_entries.py) (BibtexMulti): Information of multiple fields
- [**Filtered**](filtered.py) (BibtexFiltered): Accessing filtered information from multiple fields.

### Examples

- [**citations4text**](citations4text.py): A script to get citation numbers for a file with titles.
- [**author_relations_from_titles**](author_relations_from_titles.py) 
  - Creates a markdown file with author paper links in a mermaid graph
    - Referenced Papers and Citing Papers are also available
    - See [demo_relations](demo_relations.md) as example
  - Creates rankings of authors, cited papers and citing papers in text files

## ToDo

See [Issues](https://gitlab.com/GitPrinz/bibtex/-/issues)

- [ ] CI: run scripts to test

BibTex related

- [ ] create instances for groups?
- [ ] Inversion option for Filter
