import requests

from tqdm import tqdm

from apis.api_base import BibtexAPI, get_citation_data_dummy, add_bibtex_key

from joblib import Memory

memory = Memory('./cache/openalex', verbose=0)


def get_oa_citation_data_dummy() -> dict:
    """ Add oa specific entries"""

    citation_dict = get_citation_data_dummy()
    citation_dict.update({'author': 'NA',
                          'authors': 'NA',
                          'title': 'NA',
                          'year': 'NA',
                          'doi': 'NA',
                          'number_of_citations': 'NA',
                          'OA_relevance_score': 'NA'})

    return citation_dict


def get_oa_id(oa_link: str) -> str:

    if oa_link.startswith('https://api.openalex.org/W'):
        return oa_link[25:]
    elif oa_link.startswith('https://openalex.org/W'):
        return oa_link[21:]
    elif oa_link.startswith('W'):
        return oa_link
    else:
        raise Exception('Unknown OpenAlex link: {}'.format(oa_link))


def get_oa_work_by_link(oa_link: str) -> dict:
    """ Get citation data from open alex.

    :param oa_link: link to work or id
    :return: open alex entry
    """
    return get_oa_work_by_id(get_oa_id(oa_link))


def get_oa_work_by_id(oa_id: str) -> dict:
    """ Get citation data from open alex.

    :param oa_id: oa id starting with "W..."
    :return: open alex entry
    """

    assert oa_id[0] == 'W', 'Unknown open alex id: {}'.format(oa_id)

    # todo: add authentification
    try:
        return requests.get('https://api.openalex.org/works/'+oa_id).json()
    except Exception as err:
        print('"{}" not found'.format(oa_id))
        raise err


get_oa_work_by_id = memory.cache(get_oa_work_by_id)


def get_oa_works_by_filter(filter_str: str) -> dict:
    """ Get list of works

    :param filter_str: oa filter string
    :return: oa search query result
    """
    # todo: add authentification
    try:
        return requests.get('https://api.openalex.org/works?filter='+filter_str).json()
    except Exception as err:
        print('"{}" lead to error'.format(filter_str))
        raise err


get_oa_works_by_filter = memory.cache(get_oa_works_by_filter)


def get_oa_work_by_title(plain_title: str) -> dict:
    """ Checks Open Alex for citation data.

    Returns "most likely result"
    Will fail if nothing was found.

    :param plain_title: title without any special characters
    :return: open alex entry
    """
    title_search = 'title.search:{title}'.format(title=plain_title)
    page_limit = 'per-page:3'  # default is 25
    # identify request - will send mails to gitlab project
    # todo: make this user dependent with a settings file or else
    authentification = 'mailto:contact-project+gitprinz-bibtex-19170318-issue-@incoming.gitlab.com'

    search_query = get_oa_works_by_filter(title_search)  # todo: page_limit, authentification not sure how to join them
    if 'meta' not in search_query:
        raise Exception('Problem with query: {}'.format(search_query))
    elif search_query['meta']['count'] == 0:
        raise Exception('No Result')
    else:
        # Todo: Taking the first result without verification is risky
        #   Compare authors or other meta!
        return search_query['results'][0]


def convert_oa2cd(open_alex_entry: dict) -> dict:
    """ Get openalex citation data and convert it to "my standard" form """

    citation_dict = get_oa_citation_data_dummy()

    citation_dict.update({
        'author': open_alex_entry['authorships'][0]['author']['display_name'],
        'authors': [ent['author']['display_name'] for ent in open_alex_entry['authorships']],
        'title': open_alex_entry['display_name'],
        'year': open_alex_entry['publication_year'],
        'doi': open_alex_entry['doi'],
        'number_of_citations': open_alex_entry['cited_by_count']})

    if 'relevance_score' in citation_dict:
        citation_dict['OA_relevance_score'] = open_alex_entry['relevance_score']

    return add_bibtex_key(citation_dict)


def get_citation_data(plain_title: str, referenced_papers: bool = False, citing_papers: bool = False) -> dict:
    """ Checks Open Alex for citation data.

    Returns "most likely result"
    Will fail if nothing was found.

    :param plain_title: title without any special characters
    :param referenced_papers: add citation data of papers which were cited
    :param citing_papers: add citation data of papers which cite this paper
    :return: dict with normalized data
    """

    first_entry = get_oa_work_by_title(plain_title=plain_title)

    citation_dict = convert_oa2cd(first_entry)

    if referenced_papers:
        citation_dict['referenced_papers'] = get_referenced_papers(first_entry['referenced_works'])

    if citing_papers:
        citation_dict['citing_papers'] = get_citing_papers(first_entry['id'])

    return citation_dict


def get_referenced_papers(oa_links: list) -> list:
    """ Return citation dicts for all referenced paper to given link

    Papers with problems will be added as dummy citation (no error is raised)

    """

    paper_list = []
    for oa_link in tqdm(oa_links, 'Checking References', position=0):

        # noinspection PyUnresolvedReferences
        try:
            oa_entry = get_oa_work_by_link(oa_link)
            citation_dict = convert_oa2cd(oa_entry)

        except requests.exceptions.JSONDecodeError:

            citation_dict = get_oa_citation_data_dummy()
            citation_dict['doi'] = oa_links  # todo: bad hack

        paper_list.append(citation_dict)

    return paper_list


def get_citing_papers(oa_link: str) -> list:
    """ Return up to 25 papers citing the given paper (by open alex link)"""

    paper_list = []

    search_query = get_oa_works_by_filter('cites:{}'.format(get_oa_id(oa_link)))
    for oa_entry in tqdm(search_query['results'], 'Check Citing Papers of "{}"'.format(get_oa_id(oa_link)), position=0):
        paper_list.append(convert_oa2cd(oa_entry))

    return paper_list


class BibtexAlexHandler(BibtexAPI):

    def get_number_of_cites(self, bibtex_key: str) -> int:
        """ Checks Open Alex for number of citation.

        Returns "most likely result"
        Will fail if nothing was found.

        :param bibtex_key:
        :return: number of citations of first result with matching title
        """

        return get_citation_data(self.get_prepared_title(bibtex_key))['number_of_citations']


# testing
if __name__ == '__main__':

    test = BibtexAlexHandler('../demo.bib')

    for entry in test.entries:
        print('{key} was cited {cited} times.'.format(key=entry, cited=test.get_number_of_cites(entry)))

    print('Done')

# working nice, but finding less results than google
# oliphant2007python was cited 1518 times.
# jacso2008google was cited 148 times.
# kopka1991latex was cited 4 times.
