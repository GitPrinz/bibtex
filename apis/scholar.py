from scholarly import scholarly

from apis.api_base import BibtexAPI, get_citation_data_dummy


def get_citation_data(plain_title: str) -> dict:
    """ Checks Google Scholar for number of citation (very slow).

    Returns "most likely result"
    Will fail if nothing was found.

    :param plain_title: title without any special characters
    :return: number of citations of first result with matching title
    """

    try:
        search_query = scholarly.search_pubs(plain_title)
    except Exception as err:
        if err.args[0] == 'Cannot Fetch from Google Scholar.':
            print('Very likely a blocked connection: see in scholar.log for more Infos.')
        raise err

    if search_query is None:
        raise Exception('No Result')
    else:
        first_entry = next(search_query)
        # Todo: Taking the first result without verification is risky
        #   Compare authors or other meta!

    # ToDo: add other fields
    citation_dict = get_citation_data_dummy()
    citation_dict.update({'number_of_citations': int(first_entry['num_citations'])})
    return citation_dict


class BibtexScholarHandler(BibtexAPI):

    def get_number_of_cites(self, bibtex_key: str) -> int:
        """ Checks Google Scholar for number of citation (very slow).

        Returns "most likely result"
        Will fail if nothing was found.

        :param bibtex_key:
        :return: number of citations of first result with matching title
        """

        return get_citation_data(self.get_prepared_title(bibtex_key))['number_of_citations']


# testing
if __name__ == '__main__':

    test = BibtexScholarHandler('../demo.bib')

    for entry in test.entries:
        print('{key} was cited {cited} times.'.format(key=entry, cited=test.get_number_of_cites(entry)))

    print('Done')

# Problems since Google Scholar doesn't like robots!?
# oliphant2007python was cited 3523 times.
# jacso2008google was cited 272 times.
# kopka1991latex was cited 166 times.
