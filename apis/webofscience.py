from wos import WosClient
import wos.utils

from single_entries import BibtexSingle


class BibtexWOSHandler(BibtexSingle):

    def get_number_of_cites(self, bibtex_key: str) -> int:
        """ Checks Web of Science for number of citation (very slow).

        Returns "most likely result"
        Will fail if nothing was found.

        :param bibtex_key:
        :return: number of citations of first result with matching title
        """

        with WosClient('JohnDoe', '12345') as client:
            print(wos.utils.query(client, 'AU=Knuth Donald'))


# testing
if __name__ == '__main__':

    test = BibtexWOSHandler('../demo.bib')

    for entry in test.entries:
        print('{key} was cited {cited} times.'.format(key=entry, cited=test.get_number_of_cites(entry)))

    print('Done')

# Won't work. Even the lite Variant of Web of Science needs an paid Account.
