""" parent for all APIs to provide structure """

import abc
from typing import Union

import re

# text formatting
import string
from pylatexenc.latex2text import LatexNodes2Text

from single_entries import BibtexSingle


def get_citation_data_dummy() -> dict:
    """ Dummy method for other API"""

    return {'author': 'NotImplemented', 'title': 'NotImplemented', 'year': 'NotImplemented',
            'doi': 'NotImplemented',
            'number_of_citations': 'NotImplemented'}


def get_bibtex_key(author_name: str, year_num: Union[str, int], title_str: str) -> str:
    """ Create BibTex Key {last name}{year}{first word in title} """

    # todo: some assertions would be nice
    #   first word of title could be crap

    author_name = strip_anything(author_name).split()[-1]
    title_str = strip_anything(title_str).split()[0]

    return '{name}{year:04d}{title}'.format(name=author_name, year=int(year_num), title=title_str)


def add_bibtex_key(citation_dict: dict) -> dict:
    """ Add BibTex key in citation_dict with author, year and title """
    citation_dict['bibtex_key'] = get_bibtex_key(citation_dict['author'],
                                                 citation_dict['year'],
                                                 citation_dict['title'])
    return citation_dict


def strip_anything(text_str: str) -> str:
    """ Return text string without LaTeX, any punctuation ore single letters """

    return re.sub(r'^\w\s', ' ', re.sub(r'\s\w\s', ' ', re.sub(r'\s\w\s', ' ', re.sub(r'\s\w$', ' ',  # remove letters
                  LatexNodes2Text().latex_to_text(text_str).translate(  # remove LaTeX
                      str.maketrans({key: ' ' for key in string.punctuation +  # remove punctuation
                                     '—–‐−‑·’“'}))))))  # and special characters


class BibtexAPI(BibtexSingle):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_number_of_cites(self, bibtex_key: str) -> int:
        """ Checks some database for number of citation (very slow).

        Returns "most likely result"
        Will fail if nothing was found.

        :param bibtex_key:
        :return: number of citations of first result with matching title
        """
        pass

    def get_prepared_title(self, bibtex_key: str) -> str:
        """ Return Title without LaTeX or any punctuation """

        return strip_anything(self.get_field(bibtex_key, 'title'))


# testing
if __name__ == '__main__':

    print(strip_anything('"“test“"'))
