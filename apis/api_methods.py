""" API related helper methods """

from tqdm import tqdm

from apis.api_base import strip_anything
from apis.scholar import get_citation_data as get_citation_data_scholar
from apis.openalex import get_citation_data as get_citation_data_alex


def get_citation_data_from_file(file_name: str, api_method: str = 'OpenAlex', replacement_dict: dict = None,
                                referenced_papers: bool = False, citing_papers: bool = False) -> list:
    """ Read lines in File and get citation data for title search

    :param file_name: name of text file with titles
    :param api_method: library to get meta data from (currently only "OpenAlex" is supported
    :param replacement_dict: Phrases which shuld be replaced in titels
    :param referenced_papers: Should all references be collected? (False)
    :param citing_papers: Should all citing papers should be collected (False)

    Warning: the use of cache can lead to outdated results
    """

    # select API
    if api_method == 'OpenAlex':
        get_citation_data = get_citation_data_alex
    elif api_method == 'GoogleScholar':
        get_citation_data = get_citation_data_scholar
    else:
        raise Exception('API "{}" is unknown.'.format(api_method))

    # Check replacement_dict
    if replacement_dict is None:
        replacement_dict = dict()

    # Get lines
    with open(file_name, 'rb') as f_read:

        # everything at once, or step by step?
        #   maybe be prepared for huge files and do step by step?
        #   but this functions isn't doing anything anymore so touch the file as short as possible.
        line_contents = f_read.read().splitlines()
        citation_dicts = []

    # Get citations
    pbar = tqdm(line_contents, 'Checking Papers', position=1, leave=True)
    for line_content in pbar:

        line_content = line_content.decode("utf-8", "ignore").strip()  # remove special characters
        for key in replacement_dict:  # replace given keys
            line_content = line_content.replace(key, replacement_dict[key])
        prepared_title = strip_anything(line_content)  # more replacements

        try:
            pbar.set_description('Checking Paper: "{} ..."'.format(prepared_title[:21]))
            citation_dicts.append(get_citation_data(prepared_title,
                                                    referenced_papers=referenced_papers,
                                                    citing_papers=citing_papers))
        except Exception as err:
            if err.args[0] != 'No Result':
                raise err
            citation_dicts.append(None)

    return citation_dicts


def get_author_relations(citation_dicts: list, min_publications: int = 1) -> dict:
    """ return dict with authors and their papers

    Create dict with authors and their paper work.
    Remove authors with less than min_publications papers.

    """

    authors_dict = dict()

    for citation_dict in citation_dicts:

        if citation_dict is None:
            continue

        # add author paper links
        for author_name in citation_dict['authors']:

            if author_name not in authors_dict:
                authors_dict[author_name] = []

            # add paper note
            authors_dict[author_name].append(citation_dict['bibtex_key'])

    for author_name in authors_dict:
        if len(authors_dict[author_name]) < min_publications:
            del authors_dict[author_name]

    return authors_dict


def get_paper_groups_UNTESTED(citation_dicts: dict) -> dict:
    """ Group papers by their co-authors """

    # get new group id_function???
    group_id = 0
    paper_group_dict = dict()

    # scan for intersections
    for citation_dict in citation_dicts:

        if citation_dict is None:
            continue

        key_a = citation_dict['bibtex_key']

        # Brute Force, is there a better way?
        for citation_dict_scan in citation_dicts:
            if len(set(citation_dict['authors']) & set(citation_dict_scan['authors'])) > 0:
                # match!
                # What now!?

                key_b = citation_dict_scan['bibtex_key']

                if key_a in paper_group_dict:

                    if key_b in paper_group_dict:

                        if paper_group_dict[key_a] != paper_group_dict[key_b]:

                            raise Exception('Conflict: ToDo: Merge groups')

                elif key_b in paper_group_dict:
                    # but not key A
                    paper_group_dict[key_a] = paper_group_dict[key_b]

                else:
                    # no key is in it: Create one
                    new_key = group_id
                    group_id =+ 1  # will skip numbers after merge
                    paper_group_dict[key_a] = new_key
                    paper_group_dict[key_b] = new_key

            # no match no key?

    return paper_group_dict
