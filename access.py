import os
import bibtexparser


class BibtexFile:

    _bib_database = None
    _entries = None

    def __init__(self, filename: str):

        if not os.path.exists(filename):
            raise Exception('File not found: {}'.format(os.path.abspath(filename)))
        with open(filename, encoding='utf-8') as bibtex_file:
            self._bib_database = bibtexparser.load(bibtex_file)

    @property
    def entries(self) -> dict:
        return self._bib_database.entries_dict


# testing
if __name__ == '__main__':

    test = BibtexFile('demo.bib')

    print('There are {} entries'.format(len(test.entries)))

    print('Done')
